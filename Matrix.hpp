#include <memory>

template <typename T>
class matrix {
private:
	size_t m_size;
	std::unique_ptr<T[]> m_data;
	size_t m_leaves;
public:
	matrix(int size);
	matrix(int size, int leaves);
	matrix(const matrix<T>& rhs);
	matrix& operator=(const matrix<T>& rhs);
	void assign_value(int x, int y, int value);
	void printMat(std::ostream& os);
	T get_value(int x, int y);
	matrix<T> operator+(const matrix<T>& rhs);
	matrix<T> operator-(const matrix<T>& rhs);
	matrix<T> operator*(const matrix<T>& rhs);
	void swap(matrix<T>& rhs);
private:
	int getNewDimension();
	matrix<T> resize(int value);
	matrix<T> Strassen(const matrix<T>& rhs);
	matrix<T> multiplication(const matrix<T>& rhs);
	matrix<T> resizeBack(int value);
};







template <typename T>
matrix<T>::matrix(int size) {
	m_data = std::make_unique<T[]>(size * size);
	m_size = size;
	m_leaves = 2;
}

template <typename T>
matrix<T>::matrix(int size, int leaves) {
	m_data = std::make_unique<T[]>(size * size);
	m_size = size;
	if (leaves > 2) {
		m_leaves = leaves;
	}
	else {
		leaves = 2;
	}
}

template <typename T>
matrix<T>::matrix(const matrix<T>& rhs) {
	m_data = std::make_unique<T[]>(rhs.m_size * rhs.m_size);
	m_size = rhs.m_size;
	m_leaves = rhs.m_leaves;
	for (int i = 0; i < m_size; i++) {
		for (int j = 0; j < m_size; j++) {
			m_data[i * m_size + j] = rhs.m_data[i * m_size + j];
		}
	}
}
template <typename T>
matrix<T>& matrix<T>::operator=(const matrix<T>& rhs) {
	matrix<T> temp(rhs);
	swap(temp);
	return *this;
}

template <typename T>
void matrix<T>::swap(matrix<T>& rhs) {
	std::swap(m_data, rhs.m_data);
	std::swap(m_size, rhs.m_size);
	std::swap(m_leaves, rhs.m_leaves);
}

template <typename T>
void matrix<T>::assign_value(int x, int y, int value) {
	m_data[x * m_size + y] = value;
	int a = m_data[x * m_size + y];
}

template <typename T>
T matrix<T>::get_value(int x, int y) {
	return m_data[x * m_size + y];
}

template <typename T>
void matrix<T>::printMat(std::ostream& os) {
	for (int i = 0; i < m_size; i++) {
		for (int j = 0; j < m_size; j++) {
			T a = m_data[i * m_size + j];
			os << a << " ";
		}
		os << "\n";
	}
}
template <typename T>
matrix<T> matrix<T>::operator+(const matrix<T>& rhs) {
	matrix<T> new_mat(rhs.m_size,rhs.m_leaves);
	for (int i = 0; i < m_size; i++) {
		for (int j = 0; j < m_size; j++) {
			new_mat.assign_value(i, j, m_data[i * m_size + j] + rhs.m_data[i * m_size + j]);
		}
	}
	return new_mat;
}

template <typename T>
matrix<T> matrix<T>::operator-(const matrix<T>& rhs) {
	matrix<T> new_mat(rhs.m_size);
	for (int i = 0; i < m_size; i++) {
		for (int j = 0; j < m_size; j++) {
			new_mat.assign_value(i, j, m_data[i * m_size + j] - rhs.m_data[i * m_size + j]);
		}
	}
	return new_mat;
}

template <typename T>
matrix<T> matrix<T>::operator*(const matrix<T>& rhs) {
	matrix<T> ret(m_size);
	if (m_size <= m_leaves) {
		ret = multiplication(rhs);
	}
	else {
		int a = getNewDimension();
		if (a > m_size) {
			matrix<T> A = resize(a);
			matrix<T> B(rhs);
			B = B.resize(a);
			A = A.Strassen(B);
			ret = A.resizeBack(m_size);
		}
		else {
			ret = Strassen(rhs);
		}
	}
	return ret;
}


template <typename T>
int matrix<T>::getNewDimension() {
	int ret = 0;
	int dim = m_size;
	while (dim > m_leaves) {
		if (dim % 2 == 0) {
			dim = dim / 2;
		}
		else{
			ret += 1;
			dim = m_size + ret;
		}
	}
	return m_size + ret;
}

template <typename T>
matrix<T> matrix<T>::resize(int value) {
	matrix<T> mat(value, m_leaves);
	for (int i = 0; i < value; i++) {
		for (int j = 0; j < value; j++) {
			if (i >= m_size || j >= m_size) {
				mat.assign_value(i, j, (int)0);
			}
			else {
				mat.assign_value(i, j, m_data[i * m_size + j]);
			}
		}
	}
	return mat;
}

template <typename T>
matrix<T> matrix<T>::resizeBack(int value) {
	matrix<T> mat(value, m_leaves);
	for (int i = 0; i < value; i++) {
		for (int j = 0; j < value; j++) {
			mat.assign_value(i, j, m_data[i * m_size + j]);
		}
	}
	return mat;
}

template<typename T>
matrix<T> matrix<T>::Strassen(const matrix<T>& rhs) {
	int newSize = rhs.m_size / 2;
	matrix<T> a11(newSize, m_leaves);
	matrix<T> a12(newSize, m_leaves);
	matrix<T> a21(newSize, m_leaves);
	matrix<T> a22(newSize, m_leaves);
	matrix<T> b11(newSize, m_leaves);
	matrix<T> b12(newSize, m_leaves);
	matrix<T> b21(newSize, m_leaves);
	matrix<T> b22(newSize, m_leaves);

	

	for (int i = 0; i < newSize; i++) {
		for (int j = 0; j < newSize; j++) {
			a11.assign_value(i, j, m_data[i * m_size + j]);
			a12.assign_value(i, j, m_data[i * m_size + j + newSize]);
			a21.assign_value(i, j, m_data[(i + newSize) * m_size + j]);
			a22.assign_value(i, j, m_data[(i + newSize) * m_size + j + newSize]);
			b11.assign_value(i, j, rhs.m_data[i * m_size + j]);
			b12.assign_value(i, j, rhs.m_data[i * m_size + j + newSize]);
			b21.assign_value(i, j, rhs.m_data[(i + newSize) * m_size + j]);
			b22.assign_value(i, j, rhs.m_data[(i + newSize) * m_size + j + newSize]);
		}
	}

	matrix<T> p1 = (a11 + a22) * (b11 + b22);
	matrix<T> p2 = (a21 + a22) * (b11);
	matrix<T> p3 = (a11) * (b12 - b22);
	matrix<T> p4 = (a22) * (b21 - b11);
	matrix<T> p5 = (a11 + a12) * (b22);
	matrix<T> p6 = (a21 - a11) * (b11 + b12);
	matrix<T> p7 = (a12 - a22) * (b21 + b22);

	matrix<T> c11 = (p1 + p4 + p7 - p5);
	matrix<T> c12 = (p3 + p5);
	matrix<T> c21 = (p2 + p4);
	matrix<T> c22 = (p1 - p2 + p3 + p6);

	matrix<T> C(m_size);
	for (int i = 0; i < newSize; i++) {
		for (int j = 0; j < newSize; j++) {
			C.assign_value(i, j, c11.get_value(i, j));
			C.assign_value(i, j + newSize, c12.get_value(i, j));
			C.assign_value(i + newSize, j, c21.get_value(i, j));
			C.assign_value(i + newSize, j + newSize, c22.get_value(i, j));
		}
	}

	return C;
}

template<typename T>
matrix<T> matrix<T>::multiplication(const matrix<T>& rhs) {
	matrix<T> C(m_size, m_leaves);
	int val = 0;
	for (int i = 0; i < m_size; i++) {
		for (int j = 0; j < m_size; j++) {
			for (int k = 0; k < m_size; k++) {
				val += m_data[i * m_size + k] * rhs.m_data[k * rhs.m_size + j];
			}
			C.assign_value(i, j, val);
			val = 0;
		}
	}
	return C;
}