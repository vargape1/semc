﻿#include "Matrix.hpp"
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>


int main(int argc, char* argv[]) {
	std::string filename = "out.txt";
	bool toFile = false;
	if (argc > 1) {
		if (std::string(argv[1]) == "--help") {
			std::cout << "Program pro nasobeni ctvercovych matic\n";
			std::cout << "Prepinac -d pro vystup do souboru out.txt, za prepinacem -d jde specifikovat jiny nazev souboru\n";
			std::cout << "Na prvni radek zadejte dimenzi matic a dimenzi, kdy se pouzije zakladni nasobeni misto Strassenova algoritmu\n";
			std::cout << "Pote cisla matice oddelena mezerama po radcich\n";
			std::cout << "Po zadani obou matic probehne vypocet.\n";
			return 0;
		}
		if (std::string(argv[1]) == "-d") {
			toFile = true;
			if (argc > 2) {
				filename = std::string(argv[2]);
			}
		}
		else {
			std::cout << "Unknown argument given: " << argv[1]  <<" shutting down.\n";
			return 0;
		}
	}
	std::string line;
	int N,L;
	getline(std::cin, line);
	std::istringstream my_stream(line);
	my_stream >> N >> L ;
	if (N <= 0) {
		std::cout << "Wrong dimension";
	}
	std::cout << "Enter First matrix: \n";
	matrix<double> mat(N,L);
	double number;
	for (int i = 0; i < N; i++) {
		getline(std::cin, line);
		std::istringstream my_stream(line);
		for (int j = 0; j < N; j++) {
			my_stream >> number;
			mat.assign_value(i, j, number);
		}
	}
	std::cout << "---------------------------\n";
	std::cout << "Enter Second matrix: \n";
	matrix<double> matB(N,L);
	for (int i = 0; i < N; i++) {
		getline(std::cin, line);
		std::istringstream my_stream(line);
		for (int j = 0; j < N; j++) {
			my_stream >> number;
			matB.assign_value(i, j, number);
		}
	}

	matrix<double> C = mat * matB;
	if (toFile) {
		std::ofstream out(filename);
		C.printMat(out);
	}
	else {
		std::cout << "---------------------------\n";
		std::cout << "RESULT: \n";
		C.printMat(std::cout);
	}
	
	return 0;
}