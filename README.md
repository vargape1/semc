## Násobení matic
Program pro násobení dvou čtvercových matic o stejné dimenzi. K výpočtu je iterativně použit Strassnerův algoritmus. Je možné zadat, kdy již na místo další iterace použít normální násobení matic. K dispozici je použití přepínače --help pro nápovědu, nebo přepínače -d který přesměruje výstup do souboru out.txt, je možné za přepínačem -d přidat název souboru, který se má vytvořit.
Po zapnutí programu je potřeba zadat dimenzi matice a dimenzi, kdy se bude namísto Strassnerova algoritmu používat základní násobení matic. Následně se obě matice zadají po řádkách.
# Implementace
Implementováno pouze pro jedno vlákno. Implementace zahrnuje template classu, která jde použít i pro sčítání a násobení čtvercových matic.
